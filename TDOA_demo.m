%% Расчет РДМ
coords_1 = [5000 6000]; % Координаты постов
coords_2 = [1000 1000];
coords_3 = [9000 1000];
time_1 = 0; % Время 1 поста будет вычтено из остальных постов
time_12 = 1.47; % Разность хода 2 постов
time_13 = -13.4; % Разность хода 3 поста
seconds_in_microsecond = 1e-6;
c = 299792458; %% Скорость света
speed_of_signal = c*seconds_in_microsecond; % Скорость cигнала в м/мкс
[coords_source, d1, d2, d3] = TDOA_compute(vertcat(coords_1,coords_2,coords_3), vertcat(time_1,time_12,time_13),speed_of_signal);
disp('Координаты источника:');
disp(coords_source);
%% Визуализация
close all; figure;
grid on; hold on;
c1=circle(coords_source,d1,":");
c2=circle(coords_2,d2,"-",'r=dt_{12}\cdotc');
c3=circle(coords_3,d3,"-",'r=dt_{13}\cdotc');
p1=plot(coords_1(1), coords_1(2),Marker="o",LineWidth=3,Color=c1.Color,DisplayName="1 Пост");
p2=plot(coords_2(1), coords_2(2),Marker="o",LineWidth=3,Color=c2.Color,DisplayName="2 Пост");
p3=plot(coords_3(1), coords_3(2),Marker="o",LineWidth=3,Color=c3.Color,DisplayName="3 Пост");
p0=plot(coords_source(1), coords_source(2),Marker="o",LineWidth=4,Color="black",DisplayName="Источник");
axis equal;
legend([p1 p2 p3 p0],FontSize=10,Location="best");
xlabel('x',FontSize=14);
ylabel('y',FontSize=14,rotation=0,VerticalAlignment='middle');

function h = circle(coords,r,linestyle,varargin)
    th = 0:pi/50:2*pi;
    xunit = r * cos(th) + coords(1);
    yunit = r * sin(th) + coords(2);
    hold on;
    h = plot(xunit, yunit, LineWidth=2,LineStyle=linestyle);
    linex = [coords(1), coords(1) + r*cos(pi/3)];
    liney = [coords(2), coords(2) + r*sin(pi/3)];
    plot(linex,liney,LineWidth=2,Color=h.Color);
    if nargin > 3
        text(mean(linex), mean(liney),strcat(['\leftarrow',varargin{1}]),fontsize=14,Color="black");
    end
end
