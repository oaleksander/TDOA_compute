% Smith, J. Closed-form least-squares source location estimation from range-difference measurements.
% Вход: [(Вертикальный_вектор_из_координат_постов), (Вертикальный_вектор_из_
% времени_на_каждом_из_постов), Cкорость_распространения_сигнала]
% Выход: [Координаты_источника, *Расстояние_до_1_маяка, Расстояние_до_2_маяка, ...]
function [coords_source,varargout] = TDOA_compute(rcv_coordinates, arrival_time,speed_of_signal)
    % Проверка числа постов
    num_receivers = size(rcv_coordinates,1);
    num_dimensions = size(rcv_coordinates,2);
    if(num_receivers < num_dimensions + 1)
        warning(sprintf("Расположение источника в %d-D пространстве не может быть" + ...
            "однозначно посчитано для %d постов.",num_dimensions, num_receivers));
    end
    
    % Переход к системе координат и времени относительно главного поста
    coords_reference = rcv_coordinates(1,:);
    coords_relative = rcv_coordinates(2:end,:) - coords_reference;
    time_reference = arrival_time(1);
    time_difference = arrival_time(2:end) - time_reference;
    
    % Расстояния от главного поста до других постов
    R = sqrt(diag(coords_relative*coords_relative'));
    % Разница расстояний от ИРИ до главного и других постов
    d = time_difference*speed_of_signal;
    %Координаты постов
    S=coords_relative;
    if(rank(S) < num_dimensions)
        warning(['Неправильное расположение постов. ' ...
            'Возможно, они лежат на одной линии.'])
    end
    delta = R.^2 - d.^2;

    Swstar = pinv(S); %% Можно заменить на inv(S'*S)*S'
    % Координаты источника Rs могут быть выражены через расстояние до главного
    % приемника.
    xsfun = @(Rs) 0.5*Swstar*(delta-2*Rs*d);

    % Найдем расстояние от ИРИ до главной станции
    % "The Spherical-Intersection Metod"
    a=4-4*d'*Swstar'*Swstar*d;
    b=4*d'*Swstar'*Swstar*delta;
    c=-delta'*Swstar'*Swstar*delta;
    D = b^2-4*a*c;
    if D < 0 || a == 0
        error('Не удается определить расстояние от ИРИ до 1 приемника.');
    end
    Rs=[(-b + sqrt(D))/(2*a);
        (-b - sqrt(D))/(2*a)];
    
    % При наличии 2-х корней выбирается положительный
    if prod(Rs) < 0 
        Rs = Rs(Rs>0);
        xs = xsfun(Rs);
    else
    % Если положительных корней несколько, выбираем тот, что дает
    % наименьшую ошибку
        xs = arrayfun(xsfun,Rs,UniformOutput=false);
        e = arrayfun(@(i) norm(delta-2*Rs(i)*d-2*S*xs{i}), 1:length(Rs));
        indx = find(e==min(e));
        xs = xs{indx};
        Rs = Rs(indx);
    end
    
    if max(nargout,1)>1
        varargout = num2cell([Rs;d]);
    end

    % Переход от СК главнного приемника к мировой СК
    coords_source = coords_reference + xs';
end